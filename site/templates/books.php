<?php snippet('head') ?>
<?php snippet('header') ?>






<main class="main">
<h1>Books</h1>
  <section class="book-category young">
    <h2>For young children</h2>
      <section class="book-group wijd">
        <div class="expander">
          <div class="img-holder"></div>
          <div class="title-holder"><h3>With Julia Donaldson</h3></div>
        </div> <!--/end expander -->
        <ul>
        <!-- loop through and get all books -->
          <?php foreach($filtered = $page->grandChildren()->visible()->filterBy('bookcategory', 'youngchildrenjulia', ',') as $p): ?>
            <li>
              <a href="<?php echo $p->url() ?>">
                <!-- get the book cover TODO make responsive -->
                <?php if($image = $p->images()->findBy('imagetype', 'primarybookcover')): ?>
                  <img src="<?php echo $image->url() ?>" alt="<?php echo $p->title()->html() ?>" >
                <?php endif ?>
                <!-- /end get the book cover -->
                <!-- TODO work title into design?<?php echo $p->title()->html() ?></a> -->
                <!-- dev tool, delete for launch <?php echo $p->bookcategory()->kirbytext() ?> -->
              </a>
            </li>
          <?php endforeach ?>
      <!--/ end loop through and get all books -->
      </ul>
    </section> <!--/end book-group wijd -->
    <hr class="line" />
      <section class="book-group pap">
        <div class="expander">
          <div class="img-holder"></div>
          <div class="title-holder"><h3>Pip and Posy</h3></div>
        </div> <!--/end expander -->
        <ul>
        <!-- loop through and get all books -->
          <?php foreach($filtered = $page->grandChildren()->visible()->filterBy('bookcategory', 'youngchildrenpipposy', ',') as $p): ?>
            <li>
              <a href="<?php echo $p->url() ?>">
                <!-- get the book cover TODO make responsive -->
                <?php if($image = $p->images()->findBy('imagetype', 'primarybookcover')): ?>
                  <img src="<?php echo $image->url() ?>" alt="<?php echo $p->title()->html() ?>" >
                <?php endif ?>
                <!-- /end get the book cover -->
                <!-- TODO work title into design?<?php echo $p->title()->html() ?></a> -->
                <!-- dev tool, delete for launch <?php echo $p->bookcategory()->kirbytext() ?> -->
              </a>
            </li>
          <?php endforeach ?>
      <!--/ end loop through and get all books -->
      </ul>
    </section> <!--/end book-group pap -->
    <hr class="line" />
    <section class="book-group pap">
      <div class="expander">
        <div class="img-holder"></div>
        <div class="title-holder"><h3>With Julia Donaldson</h3></div>
      </div> <!--/end expander -->
      <ul>
      <!-- loop through and get all books -->
        <?php foreach($filtered = $page->grandChildren()->visible()->filterBy('bookcategory', 'pitcurebooksjulia', ',') as $p): ?>
          <li>
            <a href="<?php echo $p->url() ?>">
              <!-- get the book cover TODO make responsive -->
              <?php if($image = $p->images()->findBy('imagetype', 'primarybookcover')): ?>
                <img src="<?php echo $image->url() ?>" alt="<?php echo $p->title()->html() ?>" >
              <?php endif ?>
              <!-- /end get the book cover -->
              <!-- TODO work title into design?<?php echo $p->title()->html() ?></a> -->
              <!-- dev tool, delete for launch <?php echo $p->bookcategory()->kirbytext() ?> -->
            </a>
          </li>
        <?php endforeach ?>
    <!--/ end loop through and get all books -->
    </ul>
  </section> <!--/end book-group pap -->
  <hr class="line" />

</main>

<?php snippet('footer') ?>
