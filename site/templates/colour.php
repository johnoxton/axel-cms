<?php snippet('head') ?>
<?php snippet('header') ?>
  <?php snippet('prevnextcolouring') ?>

<!-- show list of all available colouring in -->
<main class="main">

	<!-- portrait or landscape -->
	<?php if($page->image()->isPortrait()): ?>
This image has been taken in portrait mode
<?php else : ?>
	Landscape
<?php endif ?>
	<!-- /end portrait or landscape -->

<img src="<?php echo $page->file()->url() ?>" alt="" /><!-- /end show list of all available colouring in -->

<?php snippet('footer') ?>
