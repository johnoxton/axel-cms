<?php snippet('head') ?>
<?php snippet('header') ?>

<!-- TODO how to check for all expired so we can do squirrel on homepage -->

<h1>What is he up to?</h1>

<!-- Show today's date
<p><?php echo "Today is " . date("l jS F Y"); ?></p>  -->
<!-- Show upcoming events -->

<!-- Loop through all events, sort by soonest, limit to 20 -->
<?php foreach(page('events')->children()->visible()->limit(10)->sortBy('eventstartdate', 'asc') as $event): ?>

<!-- if there is no event end date use the start date to set exipry date -->
<?php if($event->eventenddate()->isNotEmpty()): ?>
  <?php $expdate = strtotime($event->eventenddate()); ?>
<?php else: ?>
  <?php $expdate = strtotime($event->eventstartdate()); ?>
<?php endif ?>
<!-- Variables to work out today's date and convert $expdate -->
<?php $xpdate = date('Y-m-d', $expdate); $expirationdate = strtotime($xpdate); $todaysdate = date('Y-m-d'); $today = strtotime($todaysdate);?>
<!-- Only show events that haven't expired -->
<?php if($today < $expirationdate || $today == $expirationdate): ?>

  <article class="event">
    <div class="summary">
      <h2><?php echo $event->title()->text() ?></h2>
      <!-- Event description -->
      <?php echo $event->eventdescription()->kirbytext() ?>
      <!-- url -->
      <?php if($event->eventurl()->isNotEmpty()): ?>
      <p class="button"><a href="<?php echo $event->eventurl()->text() ?>">Vist the website <span class="hidden"><?php echo $event->eventurl()->text() ?></a></a></p>
    <?php endif ?>
      <!-- /end url -->
    </div>
    <div class="details">
      <!-- multiple or single dates -->
      <?php if($event->eventenddate()->isNotEmpty()): ?>
      <!-- start date -->
      <p class="time">
        <time datetime="2016-03-05">
          <span class="day"><?php $somedate = strtotime($event->eventstartdate()); echo date('l', $somedate); ?></span>
          <span class="num"><?php $somedate = strtotime($event->eventstartdate()); echo date('j', $somedate); ?><sup><?php $somedate = strtotime($event->eventstartdate()); echo date('S', $somedate); ?></sup>
          </span> <span class="month"><?php $somedate = strtotime($event->eventstartdate()); echo date('F', $somedate); ?></span>
          <span><?php $somedate = strtotime($event->eventstartdate()); echo date('Y', $somedate); ?></span>
        </time>
      </p>
      <!-- /end start date -->
      <!-- to and end date -->
      <p class="to">&darr; <span hidden class="hidden">to</span></p>
      <p class="time">
        <time datetime="2016-03-05">
          <span class="day"><?php $somedate = strtotime($event->eventenddate()); echo date('l', $somedate); ?></span>
          <span class="num"><?php $somedate = strtotime($event->eventenddate()); echo date('j', $somedate); ?><sup><?php $somedate = strtotime($event->eventenddate()); echo date('S', $somedate); ?></sup>
          </span> <span class="month"><?php $somedate = strtotime($event->eventenddate()); echo date('F', $somedate); ?></span>
          <span><?php $somedate = strtotime($event->eventenddate()); echo date('Y', $somedate); ?></span>
        </time>
      </p>
      <!-- /to and end date -->

      <!-- no end date -->
      <?php elseif($event->eventenddate()->empty()): ?>
        <p class="time">
          <time datetime="2016-03-05">
            <span class="day"><?php $somedate = strtotime($event->eventstartdate()); echo date('l', $somedate); ?></span>
            <span class="num"><?php $somedate = strtotime($event->eventstartdate()); echo date('j', $somedate); ?><sup><?php $somedate = strtotime($event->eventstartdate()); echo date('S', $somedate); ?></sup>
            </span> <span class="month"><?php $somedate = strtotime($event->eventstartdate()); echo date('F', $somedate); ?></span>
            <span><?php $somedate = strtotime($event->eventstartdate()); echo date('Y', $somedate); ?></span>
          </time>
        </p>
      <?php endif ?>
      <!-- /end no end date -->

      <!-- /end multiple or single dates -->

      <!-- location -->
      <?php if($event->eventaddress()->isNotEmpty()): ?>
      <p class="location"><?php echo $event->eventaddress()->text() ?></p>
    <?php endif ?>
      <!-- /end location -->

    </div>
  </article>


<?php endif ?>
  <?php endforeach ?>


<?php snippet('footer') ?>
