<?php snippet('head') ?>
<?php snippet('header') ?>
<!-- show list of all available colouring in -->
<main class="main">
	<h1>Colouring in</h1>
	<p class="intro">Grab your printer, pens, pencils and paints and get to work! Want to find out more about what Axel uses to colour in? <a href="/faqs" title="Read about Axel's illustration tools">Have a look at what he says about it</a></p>
	<ul class="circle-grid colours">
		<!-- loop through and get all images -->
	  <?php foreach(page('colouring')->children()->visible()->limit(3) as $printout): ?>
	  <li>
	    <?php if($image = $printout->images()->sortBy('sort', 'asc')->first()): ?>
	    <a href="<?php echo $printout->url() ?>">
	      <img src="<?php echo thumb($image, array('width' => 600, 'crop' => true,))->url(); ?>" alt="<?php echo $printout->title()->html() ?>" >
	    </a>
	    <?php endif ?>
	  </li>
	  <?php endforeach ?>
	</ul>
  <!--/ end loop through and get all images -->
	</ul>
</main>
<!-- /end show list of all available colouring in -->
<?php snippet('footer') ?>
