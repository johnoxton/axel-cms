<?php snippet('head') ?>

	<header class="header">
		<h1 class="axel-scheffler">Axel Scheffler</h1>
	</header>
	<main class="main">
		<nav class="nav--homepage">
			<ul>
				<li class="about">
					<a href="/about" title="Find out more about Axel">
						<span class="text">Who is he?</span>
					</a>
				</li>
				<li class="events">
					<a href="/events" title="See Axel's latest news and events">
						<span class="text">What is he up to?</span>
					</a>
				</li>
				<li class="books">
					<a href="/books" title="See the books that Axel has illustrated">
						<span class="text">Let me see the books!</span>
					</a>
				</li>

				<li class="pictures">
					<a href="/pictures" title="See some unpublished pictures Axel has drawn">
						<span class="text">Can I see some pictures?</span>
					</a>
				</li>
				<li class="colouring">
					<a href="/colouring" title="Activity sheets for colouring in">
						<span class="text">Colouring in please!</span>
					</a>
				</li>

				<li class="faqs">
					<a href="/faqs" title="Questions and answers">
						<span class="sr-only">Questions and answers</span><span class="text">?</span>
					</a>
				</li>
				<li class="gruffalo">
					<a href="/books/gruffalo" title="About the Gruffalo">
						<span class="text">The Gruffalo</span>
					</a>
				</li>
			</ul>
		</nav>
	</main>

<?php snippet('footer') ?>
