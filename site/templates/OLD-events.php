<?php snippet('head') ?>
<?php snippet('header') ?>

<!-- TODO how to check for all expired so we can do squirrel on homepage -->

<h1>Events</h1>

<!-- Show today's date
<p><?php echo "Today is " . date("l jS F Y"); ?></p>  -->
<!-- Show upcoming events -->

<!-- Loop through all events, sort by soonest, limit to 20 -->
<?php foreach(page('events')->children()->limit(20)->sortBy('eventstartdate', 'desc') as $event): ?>

<!-- if there is no event end date use the start date to set exipry date -->
<?php if($event->eventenddate()->isNotEmpty()): ?>
  <?php $expdate = strtotime($event->eventenddate()); ?>
<?php else: ?>
  <?php $expdate = strtotime($event->eventstartdate()); ?>
<?php endif ?>
<!-- Variables to work out today's date and convert $expdate -->
<?php

  $xpdate = date('Y-m-d', $expdate);
  $expirationdate = strtotime($xpdate);
  //echo "expires " . $expirationdate
  //echo "exp date" . $xpdate . "<br />";
  $todaysdate = date('Y-m-d');
  //echo "todays date" . $todaysdate . "<br />";
  $today = strtotime($todaysdate);
  //echo " today" . $today;
?>
<!-- Only show events that haven't expired -->
<?php if($today < $expirationdate): ?>
  <!-- Event title -->
  <h2><?php echo $event->eventtitle()->html() ?></h2>
  <!-- Event description -->
  <p><?php echo $event->eventdescription()->kirbytext() ?></p>
<?php endif ?>












  <?php endforeach ?>


<?php snippet('footer') ?>
