<?php snippet('head') ?>
<?php snippet('header') ?>
</div><!--/wrapper-->
  <main>
    <div class="wrapper">
      <div class="main">
        <p class="back"><a href="/books/" title="Back to books">&larr; Books</a></p>
        <article>
          <div class="intro">
            <h1><?php echo $page->title()->html() ?></h1>
            <p class="meta">Written by <?php echo $page->author()->text() ?></p>
            <?php echo $page->blurb()->kirbytext() ?>
          </div> <!-- /end .intro -->

          <?php if($image = $page->files()->findBy('imagetype', 'primarybookcover')): ?>
          <div class="cover">
            <img src="<?php echo thumb($image, array('width' => 300,))->url(); ?>"/>
          </div> <!-- /end .cover -->
          <?php endif ?>

          <!-- /check for feature-image -->
          <?php if($page->files()->findBy('imagetype', 'featureimg')): ?>
          </article><!--/article-->
          </div><!--/main-->
        </div><!--/wrapper--><!-- breaking wrapper and main here to let featured image sit outside the grid -->
        <div class="feature-image">
          <img src="<?php echo $page->files()->findBy('imagetype', 'featureimg')->url() ?>" alt="" />
        </div>
        <!-- start wrappper, main and article again -->
        <div class="wrapper">
          <div class="main">
          <?php endif ?>
        <!-- /end feature-image -->










<div class="clear:both;">
<!-- Axel quote -->
<?php if($page->axelquote()->isNotEmpty()): ?>
  <!-- get authors potrait -->
      <img src="/assets/portraits/axel.jpg"/>
    <!--/end get authors potrait -->
	<h1>Axel says:</h1>
	<blockquote><?php echo $page->axelquote()->kirbytext() ?></blockquote>
<?php endif ?>
<!-- /end Axel quote -->
</div>

<!-- Author quote -->
<?php if($page->authorquote()->isNotEmpty()): ?>

	<!-- get authors potrait -->
    	<img src="/assets/portraits/<?php echo $page->authorfriendlyname()->value() ?>.jpg"/>
    <!--/end get authors potrait -->

	<?php if($page->authorfriendlyname()->isNotEmpty()): ?>
		<h1><?php echo $page->authorfriendlyname()->text() ?> says:</h1>
	<?php else: ?>
	<h1><?php echo $page->author()->text() ?> says:</h1>
	<?php endif ?>
	<blockquote><?php echo $page->authorquote()->kirbytext() ?></blockquote>
<?php endif ?>
<!-- /end Author quote -->

    </div>
      <ul>
  <?php foreach($page->files()->filterBy('imagetype', 'altbookcover') as $file): ?>
  <li>
    <img src="<?php echo $file->url() ?>"/>
      <!-- delete me <?php echo html($file->filename()) ?> -->
      <?php echo $file->caption()->kirbytext() ?>
  </li>
  <?php endforeach ?>
</ul>

  <ul>
  <?php foreach($page->files()->filterBy('imagetype', 'sketch')->sortBy('sort', 'asc') as $file): ?>
  <li>
    <img src="<?php echo $file->url() ?>"/>
      <!-- delete me <?php echo html($file->filename()) ?> -->
      <?php echo $file->caption()->kirbytext() ?>
  </li>
  <?php endforeach ?>
</ul>

<!-- Where to buy (primary) -->
<?php if(!$page->wheretobuyone()->empty()): ?>
	<h1>Where to buy in the UK</h1>
	<?php if(!$page->wheretobuyonebuttontext()->empty()): ?>
		<p><a href="<?php echo $page->wheretobuyone()->toUrl() ?>">Buy <i><?php echo $page->title()->text() ?></i> from <?php echo $page->wheretobuyonebuttontext()->text() ?></a></p>
	<?php else: ?>
	<p><a href="<?php echo $page->wheretobuyone()->toUrl() ?>">Buy <i><?php echo $page->title()->text() ?></i></a></p>
	<?php endif ?>
	<blockquote></blockquote>
<?php endif ?>
<!-- /end Where to buy (primary) -->
<!-- Where to buy (alternative) -->
<?php if(!$page->wheretobuyaltone()->empty()): ?>
	<h3>Other ways to buy</h3>
	<?php if(!$page->wheretobuyaltonebuttontext()->empty()): ?>
		<h1><?php echo $page->wheretobuyaltonebuttontext()->text() ?></h1>
	<?php else: ?>
	<h1>DEFAULT WHERE TO BUY</h1>
	<?php endif ?>
	<blockquote><?php echo $page->wheretobuyaltone()->kirbytext() ?></blockquote>
<?php endif ?>
<!-- /end Where to buy (alternative) -->
  </main>

  <?php snippet('prevnextbooks') ?>
            <article>
<?php snippet('footer') ?>

<!-- dev stuff todo: delete -->
<!-- for dev <?php echo $page->bookcategory()->kirbytext() ?> -->
<!-- <p>Published by <?php echo $page->publisher()->text() ?></p> -->
