<?php if(!defined('KIRBY')) exit ?>

title: Timeline item
pages: false
files:
  type:image
  sortable: true
  fields:
    imagetype:
      label: Type of image
      type: select
      required: true
      options:
        primarybookcover: Primary book cover
        altbookcover: Alternative book cover
        sketch: Sketch
        backgroundimg: Background image
        authorimg: Author image
    caption:
      label: Image caption
      help: This fields is optional and should only be added to sketches or alternative book covers
      type: text
fields:
  bookdetails:
    label: Basic timeline entry
    type: headline
    help: All fields here are required.
  aboutmaintext:
    label: Main text
    type:  textarea
    required: true
    buttons: false
