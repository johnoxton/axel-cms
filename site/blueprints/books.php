<?php if(!defined('KIRBY')) exit ?>

title: Books
deletable: false
preview: false
files: false
pages: true
  max: 6
  template:
    - book-parent
fields:
  bookdetails:
    label: Books
    type: info
    text: >
      Select a parent level from the menu on the left


      ![Alt text](http://placehold.it/350x150)
