<?php if(!defined('KIRBY')) exit ?>

title: Book
preview: false
pages: true
  template:
    - book
files: false
fields:
  bookdetails:
    label: Books
    type: info
    text: >
      The main books page is generated automatically. Please add or edit books via the Pages section.

      INSERT TUTORIAL FOR ADDING BOOKS HERE


      ![Alt text](http://placehold.it/350x150)
