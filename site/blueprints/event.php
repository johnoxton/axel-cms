<?php if(!defined('KIRBY')) exit ?>

title: Events
pages: false
fields:
  basiceventinformation:
    label: Basic event information
    type: headline
  title:
    label: Title
    type:  text
    required: true
  eventdescription:
    label: Description
    type:  textarea
    buttons: false
    required: true
    help: You can use Markdown syntax to create more complex entries. See the tutorial for more info.
  eventdatesandtimes:
    label: Event dates, times and location
    type: headline
    help: Minimum required information is 'Event start date' all other fields are optional.
  eventstartdate:
    label: Event start date
    type: date
    format: DD/MM/YYYY
    width: 1/2
    required: true
  eventenddate:
    label: Event end date
    type: date
    format: DD/MM/YYYY
    width: 1/2
    help: Optional. Leave this blank if it is a 1 day event i.e. the start and end dates are the same
  eventstarttime:
    label: Event start time
    type:  time
    help: Optional.
    width: 1/2
    time:
      format: 12
      interval: 15
  eventendtime:
    label: Event end time
    type:  time
    help: Optional.
    width: 1/2
    time:
      format: 12
      interval: 15
  eventaddress:
    label: Event location or full address
    type:  text
    help: e.g. 'Wray Castle, Lake Windemere' or 'OK HQ, 7 Sheep Street, Shipston on Stour, Warwickshire, CV36 4AE'.
  eventurl:
    label: Website address
    type:  url
    help: Optional. Must include the http://
