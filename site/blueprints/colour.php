<?php if(!defined('KIRBY')) exit ?>

title: Colouring in
pages: false
fields:
  imagetitle:
    label: Title
    type:  text
    required: true
    help: This will be displayed on the main print page and as alt text to aid accessibility
