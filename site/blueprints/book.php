<?php if(!defined('KIRBY')) exit ?>

title: Book
pages: false
files:
  type:image
  sortable: true
  fields:
    imagetype:
      label: Type of image
      type: select
      required: true
      options:
        default:
        primarybookcover: Primary book cover
        altbookcover: Alternative book cover
        sketch: Sketch
        featureimg: Feature image
        authorimg: Author image
    caption:
      label: Image caption
      help: This fields is optional and should only be added to sketches or alternative book covers
      type: text
fields:
  bookdetails:
    label: Basic book details
    type: headline
    help: All fields here are required.
  title:
    label: Book title
    type:  text
    required: true
  author:
    label: Author(s) full name
    help: e.g. Philip Ardagh/Philip Ardagh and Another Author/Author 1, Author 2 and Author 3
    type:  text
    required: true
  publisher:
    label: Publisher
    type:  text
    required: true
  blurb:
    label: Publisher's blurb
    type:  textarea
    required: true
    buttons: false
  bookcategory:
    label: Category
    type:  select
    placeholder: Please select a category
    required: true
    options:
      default:
      youngchildrenjulia: For young children - with Julia Donaldson
      youngchildrenpipposy: For young children - Pip and Posy
      youngchildrenother: For young children - Other books for young children
      pitcurebooksjulia: Picture books - with Julia Donaldson
      pitcurebooksother: Picture books - Other
      olderchildren: For older children
  bookquotes:
    label: Quotes
    type: headline
    help: These fields are optional, if left blank no quotes will show on the page.
  axelquote:
    label: Axel says quote
    type:  textarea
    buttons: false
    help: Do not add 'axel says' or quotation marks, they will be added automagically when you save!
  line-a:
    type: line
  authorquote:
    label: Author quote
    type:  textarea
    buttons: false
    help: Do not add 'author says' or quotation marks, they will be added automagically when you save!
  authorfriendlyname:
    label: Which author is this quote from?
    type:  select
    help: This option is needed so the correct author portrait is shown and just their first name. If you need to add more authors to this list email thedesk@mrandmrsok.com
    options:
      Julia: Julia Donaldson
      Philip: Philip Ardagh
  buyingoptions:
    label: Where to buy in the UK
    type: headline
    help: These fields are optional, if left blank no buy options will appear on the page
  wheretobuyone:
    label: Where to buy in the UK
    type:  url
  wheretobuyonebuttontext:
    label: Button friendly website address for 'Where to buy (primary)''
    help: e.g. 'hive.co.uk', or 'Hive'. If left blank will default to 'Buy 'book name''
    type:  text
  altbuyingoptions:
    label: Other ways to buy
    type: headline
    help: These fields are optional, if left blank no buy options will appear on the page
  wheretobuyaltone:
    label: Where to buy (alt 1)
    type:  url
  wheretobuyaltonebuttontext:
    label: Button friendly website address (alt 1)
    help: e.g. 'Barnes &amp; Noble', or 'barnesandnoble.com'. If left blank will default to 'Alternative way to buy this book'
    type:  text
  line-b:
    type: line
  wheretobuyalttwo:
    label: Where to buy (alt 2)
    type:  url
  wheretobuyalttwobuttontext:
    label: Button friendly website address (alt 2)
    help: e.g. 'Barnes &amp; Noble (USA)', or 'barnesandnoble.com'. If left blank will default to 'Alternative way to buy this book'
    type:  text
