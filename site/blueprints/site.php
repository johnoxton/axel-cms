<?php if(!defined('KIRBY')) exit ?>

title: Site
pages: false
fields:
  title:
    label: Title
    type:  text
  author:
    label: Author
    type:  text
  description:
    label: Description
    type:  textarea
    buttons: false
  keywords:
    label: Keywords
    type:  tags
  copyright:
    label: Copyright
    type:  textarea
    buttons: false