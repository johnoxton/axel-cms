<?php if($page->hasPrevVisible()): ?>
<a href="<?php echo $page->prevVisible()->url() ?>">Previous book</a>
<?php endif ?>
<p class="back"><a href="/books/" title="Back to books">&larr; all Books</a></p>
<?php if($page->hasNextVisible()): ?>
<a href="<?php echo $page->nextVisible()->url() ?>">Next  book</a>
<?php endif ?>
