<?php if($page->hasPrevVisible()): ?>
<a href="<?php echo $page->prevVisible()->url() ?>">Previous Colouring in</a>
<?php endif ?>
<p class="back"><a href="/colouring/" title="Back to colouring in">&larr; all Colouring in</a></p>
<?php if($page->hasNextVisible()): ?>
<a href="<?php echo $page->nextVisible()->url() ?>">Next  Colouring in</a>
<?php endif ?>
