<header class="header--standard">
	<div class="home-holder">
		<a href="/" title="Return to Axel Scheffler's homepage" class="home">Home</a>
	</div>
	<div class="name-holder">
		<a href="/" title="Return to Axel Scheffler's homepage" class="axel-scheffler">Axel Scheffler</a>
	</div>
</header>
<main class="main">
