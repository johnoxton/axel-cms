<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <title>Axel Scheffler</title>

		<!--Google fonts -->
		<link href='https://fonts.googleapis.com/css?family=Caudex:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" href="/assets/stylesheets/main.css" />

		<!-- Modernizr TODO switch to production build -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

				<script type="text/javascript">
function ImagetoPrint(source) {
    return "<html><head><script>function step1(){\n" +
            "setTimeout('step2()', 10);}\n" +
            "function step2(){window.print();window.close()}\n" +
            "</scri" + "pt></head><body onload='step1()'>\n" +
            "<img src='" + source + "' /></body></html>";
}
function PrintImage(source) {
    Pagelink = "about:blank";
    var pwa = window.open(Pagelink, "_new");
    pwa.document.open();
    pwa.document.write(ImagetoPrint(source));
    pwa.document.close();
}
</script>
</head>
<body class=“<?php echo $page->template() ?>”>
	<div class="page-wrap">
	<div class="wrapper">
