</div><!--/page-wrap -->
<footer class="footer">
  <div class="wrapper">
    <div class="footer-first">
      <p class="extra-small">All content &copy; Axel Scheffler 2016</p>
    </div>
    <div class="footer-second">
      <p class="extra-small">An <a href="http://mrandmrsok.com" title="Mr &amp; Mrs OK's website">OK</a> site</p>
    </div>
  </div><!--/wrapper-->
</footer>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/assets/js/source/vendor/expandy.js"></script>
<script>
        $('.book-category').makeExpander({
            toggleElement: 'div.expander',
            jqAnim: true,
            showFirst: false,
            accordion: true,
            speed: 500,
            indicator: false
        });


</script>
<script src="/assets/js/source/vendor/multicolumn.js"></script>
<script>
if (!Modernizr.csscolumns) {
   $('.two-columns').multicolumn();
 }
 </script>
</body>
</html>
