<?php if(!defined('KIRBY')) exit ?>

username: johnoxtonking
email: john@mrandmrsok.com
password: >
  $2a$10$l8rx5skB4jFez4y1251PTu5tJOhp.LV/Ubl9IO0JV3nEwSPqXiJaC
language: en
role: admin
history:
  - >
    events/sample-multiday-event-with-markdown-formatting
  - events/a-default-simple-single-day-event
  - colouring/test-colouring-2
  - books/picture-books-with-julia-donaldson/the-gruffalo
